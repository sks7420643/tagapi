import kame.tagapi.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class TileEntityTest {

    static {
        _ServerMain.waitInitialize();
    }

    static Material[] types() throws ExecutionException, InterruptedException {
        var world = Bukkit.getWorlds().stream().findFirst().orElseThrow();
        var future = new FutureTask<>(() -> {
            var list = new ArrayList<Material>();
            var loc = new Location(world, 0, 0, 0);
            for (var type : Material.values()) {
                try {
                    if (!type.isBlock()) continue;
                    world.getBlockAt(loc).setType(type);
                    if (new TagTileEntity(world.getBlockAt(loc)).getSection() == null) continue;
                    list.add(type);
                } catch (Exception e) {
                    //
                }
            }
            return list.stream();
        });
        var plugin = Objects.requireNonNull(Bukkit.getPluginManager().getPlugin("Dummy"));
        Bukkit.getScheduler().runTask(plugin, future);
        while(!future.isDone() && !future.isCancelled()) Thread.yield();
        return future.get().toArray(Material[]::new);
    }

    @ParameterizedTest
    @MethodSource("types")
    void test(Material type) throws Exception {
        // 全Entityを対象にTagEntityを作成するテスト
        var world = Bukkit.getWorlds().stream().findFirst().orElseThrow();
        var loc = new Location(world, 0, 0, type.ordinal());
        var future = new FutureTask<>(() -> test(loc, type));
        var plugin = Objects.requireNonNull(Bukkit.getPluginManager().getPlugin("Dummy"));
        Bukkit.getScheduler().runTask(plugin, future);
        while(!future.isDone() && !future.isCancelled()) Thread.yield();
        var result = future.get();
        if (result != null) throw result;
    }

    @Test
    void test() throws Exception {
        var world = Bukkit.getWorlds().stream().findFirst().orElseThrow();
        var loc = new Location(world, 0, 0, 0);
        var future = new FutureTask<>(() -> test(loc));
        var plugin = Objects.requireNonNull(Bukkit.getPluginManager().getPlugin("Dummy"));
        Bukkit.getScheduler().runTask(plugin, future);
        while(!future.isDone() && !future.isCancelled()) Thread.yield();
        var result = future.get();
        if (result != null) throw result;
    }

    Exception test(Location loc, Material type) {
        try {
            var world = Objects.requireNonNull(loc.getWorld());
            world.getBlockAt(loc).setType(type);
            var tagTile = new TagTileEntity(world.getBlockAt(loc));
            System.out.println("Tag (" + type + ") " + tagTile.getSection());
            return null;
        } catch (Exception e) {
            return e;
        }
    }

    Exception test(Location loc) {
        try {
            var world = Objects.requireNonNull(loc.getWorld());
            world.getBlockAt(loc).setType(Material.COMMAND_BLOCK);
            var tagTile = new TagTileEntity(world.getBlockAt(loc));
            System.out.println("初期値 (" + Material.COMMAND_BLOCK + ") " + tagTile.getSection());
            Objects.requireNonNull(tagTile.getSection()).put("Command", new TagString("aaa"));
            tagTile.save();
            var valid = Objects.requireNonNull(new TagTileEntity(world.getBlockAt(loc)).getSection());
            System.out.println("書込値 (" + Material.COMMAND_BLOCK + ") " + tagTile.getSection());
            System.out.println("読込値 (" + Material.COMMAND_BLOCK + ") " + valid);
            var validate = valid.get("Command") instanceof TagString d ? d.data() : "";
            Assertions.assertEquals("aaa", validate);
            return null;
        } catch (Exception e) {
            return e;
        }
    }
}
