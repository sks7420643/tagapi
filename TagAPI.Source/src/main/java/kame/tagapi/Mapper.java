package kame.tagapi;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Stream;

class Mapper {
    // TODO:現状ではバージョン変更で動作しなくなる可能性があるのでなんとかする。
    //      (各処理を抽象化してバージョンごとに切り替えるのを検討中)
    private static final String VERSION = Bukkit.getServer().getClass().getPackage().getName();
    //ClassCache----------------------------------------------
    private static final Map<Class<?>, NBTTag> nmsCache = new HashMap<>();
    private static final Map<Class<? extends TagBase>, NBTTag> tagCache = new HashMap<>();
    //NBTBase------------------------------------------------------
    private static final Class<?> NBTBase_Class = Internal.getClass("net.minecraft.nbt.NBTBase");
    private static final Method NBTBase_Method_getTypeId = Internal.findMethod(NBTBase_Class, byte.class);
    //NMSItemStack-------------------------------------------------
    private static final Class<?> MinecraftItemStack_Class = Internal.getClass("net.minecraft.world.item.ItemStack");
    private static final Field MinecraftItemStack_Field_nbt = Internal.findField(MinecraftItemStack_Class, NBTTag.Compound.nmsType);
    //CraftItemStack-----------------------------------------------
    private static final Class<?> CraftItemStack_Class = Internal.getClass(VERSION + ".inventory.CraftItemStack");
    private static final Method CraftItemStack_Method_asNMSCopy = Internal.getMethod(CraftItemStack_Class, "asNMSCopy", ItemStack.class);
    private static final Method CraftItemStack_Field_getItemMeta = Internal.getMethod(CraftItemStack_Class, "getItemMeta", MinecraftItemStack_Class);
    //NMSEntity----------------------------------------------------
    private static final Class<?> MinecraftEntity_Class = Internal.getClass("net.minecraft.world.entity.Entity");
    private static final Method MinecraftEntity_Method_getNBT = Internal.findMethod(MinecraftEntity_Class, NBTTag.Compound.nmsType, NBTTag.Compound.nmsType);
    private static final Method MinecraftEntity_Method_setNBT = Internal.getMethod(MinecraftEntity_Class, "g", NBTTag.Compound.nmsType);
    //CraftEntity--------------------------------------------------
    private static final Class<?> CraftEntity_Class = Internal.getClass(VERSION + ".entity.CraftEntity");
    private static final Method CraftEntity_Method_getHandle = Internal.getMethod(CraftEntity_Class, "getHandle");
    //NMSBlockPosition---------------------------------------------
    private static final Class<?> MinecraftBlockPosition_Class = Internal.getClass("net.minecraft.core.BlockPosition");
    private static final Constructor<?> MinecraftBlockPosition_Constructor = Internal.getConstructor(MinecraftBlockPosition_Class, int.class, int.class, int.class);
    //NMSTileEntity------------------------------------------------
    private static final Class<?> MinecraftTileEntity_Class = Internal.getClass("net.minecraft.world.level.block.entity.TileEntity");
    private static final Method MinecraftTileEntity_Method_getNBT = Internal.findMethod(MinecraftTileEntity_Class, NBTTag.Compound.nmsType);
    private static final Method MinecraftTileEntity_Method_setNBT = Internal.getMethod(MinecraftTileEntity_Class, "a", NBTTag.Compound.nmsType);
    //NMSIBlockAccess----------------------------------------------
    private static final Class<?> MinecraftIBlockAccess_Class = Internal.getClass("net.minecraft.world.level.IBlockAccess");
    private static final Method MinecraftIBlockAccess_Method_getTileEntity = Internal.findMethod(MinecraftIBlockAccess_Class, MinecraftTileEntity_Class, MinecraftBlockPosition_Class);
    //CraftBlock---------------------------------------------------
    private static final Class<?> CraftBlock_Class = Internal.getClass(VERSION + ".block.CraftBlock");
    private static final Method CraftBlock_Method_getHandle = Internal.getMethod(CraftBlock_Class, "getHandle");
    //MojangsonParser----------------------------------------------
    private static final Class<?> StringReader_Class = Internal.getClass("com.mojang.brigadier.StringReader");
    private static final Constructor<?> StringReader_Constructor = Internal.getConstructor(StringReader_Class, String.class);
    private static final Class<?> MojangsonParser_Class = Internal.getClass("net.minecraft.nbt.MojangsonParser");
    private static final Constructor<?> MojangsonParser_Constructor = Internal.getConstructor(MojangsonParser_Class, StringReader_Class);
    private static final Method MojangsonParser_Method_parse = Internal.getMethod(MojangsonParser_Class, "d");

    @NotNull
    public static Object toNms(TagBase value) {
        if (value == null) {
            throw new NullPointerException("nbt");
        } else if (value instanceof TagData<?> data) {
            return tagCache.get(data.getClass()).newInstance(data.getData());
        } else if (value instanceof TagList data) {
            return NBTTag.List.newInstance(data.nmsList, getListTypeId(data.nmsList));
        } else if (value instanceof TagSection data) {
            return NBTTag.Compound.newInstance(data.nmsMap);
        } else {
            throw new IllegalArgumentException(value.getClass().getName());
        }
    }

    @Nullable
    public static TagBase toTag(Object nbt) {
        return nbt != null ? nmsCache.get(nbt.getClass()).newApiInstance(nbt) : null;
    }

    public static byte getListTypeId(List<Object> data) {
        return getType(data).map(x -> x.id).orElse((byte) 0);
    }

    @NotNull
    public static Class<? extends TagBase> getListType(List<Object> data) {
        var result = getType(data).map(x -> x.apiType).orElse(null);
        return result != null ? result : TagBase.class;
    }

    private static Optional<NBTTag> getType(List<Object> data) {
        return data.stream().filter(Objects::nonNull).flatMap(Mapper::getTypes).findFirst();
    }

    private static Stream<NBTTag> getTypes(Object data) {
        return Arrays.stream(NBTTag.values()).filter(x -> x.isNmsInstance(data));
    }

    public static TagBase parse(String nbtString) throws Exception {
        var reader = Internal.newInstance(StringReader_Constructor, nbtString);
        var parser = Internal.newInstance(MojangsonParser_Constructor, reader);
        return toTag(MojangsonParser_Method_parse.invoke(parser));
    }

    public static Object asNMSCopy(ItemStack item) {
        return Internal.invoke(CraftItemStack_Method_asNMSCopy, null, item);
    }

    public static Object getNBTField(Object nmsItem) {
        var data = Internal.get(MinecraftItemStack_Field_nbt, nmsItem);
        if (data == null) {
            Internal.set(MinecraftItemStack_Field_nbt, nmsItem, data = toNms(new TagSection()));
        }
        return data;
    }

    public static ItemMeta getItemMeta(Object item) {
        return Internal.invoke(CraftItemStack_Field_getItemMeta, null, item);
    }

    public static Object getEntityData(Entity entity) {
        var nbt = toNms(new TagSection());
        if (CraftEntity_Class.isInstance(entity)) {
            var nmsEntity = Internal.invoke(CraftEntity_Method_getHandle, entity);
            return Internal.invoke(MinecraftEntity_Method_getNBT, nmsEntity, nbt);
        }
        return nbt;
    }

    public static void setEntityData(Entity entity, Object nbt) {
        if (CraftEntity_Class.isInstance(entity)) {
            var nmsEntity = Internal.invoke(CraftEntity_Method_getHandle, entity);
            Internal.invoke(MinecraftEntity_Method_setNBT, nmsEntity, nbt);
        }
    }

    public static Object getTileEntity(Block block) {
        if (CraftBlock_Class.isInstance(block)) {
            var handle = Internal.invoke(CraftBlock_Method_getHandle, block);
            var pos = Internal.newInstance(MinecraftBlockPosition_Constructor, block.getX(), block.getY(), block.getZ());
            return Internal.invoke(MinecraftIBlockAccess_Method_getTileEntity, handle, pos);
        }
        return null;
    }

    public static Object getTileEntityData(Object tileEntity) {
        return Internal.invoke(MinecraftTileEntity_Method_getNBT, tileEntity);
    }

    public static void setTileEntityData(Object tileEntity, Object nbt) {
        Internal.invoke(MinecraftTileEntity_Method_setNBT, tileEntity, nbt);
    }

    private enum NBTTag {
        Byte("net.minecraft.nbt.NBTTagByte", TagByte.class, byte.class, byte.class),
        Short("net.minecraft.nbt.NBTTagShort", TagShort.class, short.class, short.class),
        Int("net.minecraft.nbt.NBTTagInt", TagInt.class, int.class, int.class),
        Long("net.minecraft.nbt.NBTTagLong", TagLong.class, long.class, long.class),
        Float("net.minecraft.nbt.NBTTagFloat", TagFloat.class, float.class, float.class),
        Double("net.minecraft.nbt.NBTTagDouble", TagDouble.class, double.class, double.class),
        String("net.minecraft.nbt.NBTTagString", TagString.class, String.class, String.class),
        List("net.minecraft.nbt.NBTTagList", TagList.class, Object.class, List.class, byte.class),
        Compound("net.minecraft.nbt.NBTTagCompound", TagSection.class, Object.class, Map.class),
        ByteArray("net.minecraft.nbt.NBTTagByteArray", TagByteArray.class, byte[].class, byte[].class),
        IntArray("net.minecraft.nbt.NBTTagIntArray", TagIntArray.class, int[].class, int[].class),
        LongArray("net.minecraft.nbt.NBTTagLongArray", TagLongArray.class, long[].class, long[].class),
        ;

        public final byte id;
        private final Class<? extends TagBase> apiType;
        private final Class<?> nmsType;
        private final Constructor<?> nms_constructor;
        private final Constructor<?> api_constructor;
        private final Field inner;

        NBTTag(String name, Class<? extends TagBase> api, Class<?> data, Class<?>... init) {
            nmsType = Internal.getClass(name);
            id = Internal.invoke(NBTBase_Method_getTypeId, Internal.allocInstance(nmsType));
            apiType = api;
            inner = Internal.findField(nmsType, init[0]);
            nms_constructor = Internal.getConstructor(nmsType, init);
            api_constructor = Internal.getConstructor(apiType, data);
            nmsCache.put(nmsType, this);
            tagCache.put(api, this);
        }

        public boolean isNmsInstance(Object nms) {
            return nmsType.isInstance(nms);
        }

        public <T> T getInner(Object nms) {
            return Internal.get(inner, nms);
        }

        public Object newInstance(Object... data) {
            return Internal.newInstance(nms_constructor, data);
        }

        public <T> T newApiInstance(Object data) {
            return Internal.newInstance(api_constructor, (Object) getInner(data));
        }
    }

    @SuppressWarnings("unchecked")
    private static final class Internal {

        private static final Object unsafe = get(getField(getClass("sun.misc.Unsafe"), "theUnsafe"), null);
        private static final Method alloc = getMethod(getClass("sun.misc.Unsafe"), "allocateInstance", Class.class);

        private static Class<?> getClass(String name) {
            try {
                return Class.forName(name);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        private static Constructor<?> getConstructor(Class<?> c, Class<?>... params) {
            try {
                var r = c.getDeclaredConstructor(params);
                if (!Modifier.isPublic(r.getModifiers())) r.setAccessible(true);
                return r;
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }

        private static Field getField(Class<?> c, String name) {
            try {
                var f = c.getDeclaredField(name);
                if (!Modifier.isPublic(f.getModifiers())) f.setAccessible(true);
                return f;
            } catch (NoSuchFieldException e) {
                throw new RuntimeException(e);
            }
        }

        private static Field findField(Class<?> c, Class<?> type) {
            var f = Arrays.stream(c.getDeclaredFields())
                    .filter(x -> !Modifier.isStatic(x.getModifiers()))
                    .filter(x -> x.getType() == type)
                    .findFirst()
                    .orElseThrow();
            if (!Modifier.isPublic(f.getModifiers())) f.setAccessible(true);
            return f;
        }

        private static Method getMethod(Class<?> c, String name, Class<?>... args) {
            try {
                var m = c.getDeclaredMethod(name, args);
                if (!Modifier.isPublic(m.getModifiers())) m.setAccessible(true);
                return m;
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }

        private static Method findMethod(Class<?> c, Class<?> returnType, Class<?>... params) {
            var m = Arrays.stream(c.getDeclaredMethods())
                    .filter(x -> !Modifier.isStatic(x.getModifiers()))
                    .filter(x -> returnType.equals(x.getReturnType()))
                    .filter(x -> Arrays.equals(x.getParameterTypes(), params))
                    .findFirst()
                    .orElseThrow();
            if (!Modifier.isPublic(m.getModifiers())) m.setAccessible(true);
            return m;
        }

        private static <T> T get(Field field, Object obj) {
            try {
                return (T) field.get(obj);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

        private static void set(Field field, Object obj, Object data) {
            try {
                field.set(obj, data);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

        private static <T> T invoke(Method method, Object obj, Object... params) {
            try {
                return (T) method.invoke(obj, params);
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }

        private static <T> T newInstance(Constructor<?> constructor, Object... params) {
            try {
                return (T) constructor.newInstance(params);
            } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
                throw new RuntimeException(e);
            }
        }

        private static <T> T allocInstance(Class<T> type) {
            try {
                return (T) alloc.invoke(unsafe, type);
            } catch (InvocationTargetException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
