package kame.tagapi.convertion;

import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * クラスへの変換時に使用するオプションを提供するクラスです
 */
public class ConvertOptions {

    /**
     * ConvertOptionsのデフォルトを返します
     */
    public static final ConvertOptions DEFAULT = new ConvertOptions();

    private final Map<Class<?>, Class<?>> knownClass = new HashMap<>();

    boolean isIgnoreCase = false;

    /**
     * ConvertOptionsの初期化をします
     */
    public ConvertOptions() {
        registerClass(Iterable.class, ArrayList.class);
        registerClass(Collection.class, ArrayList.class);
        registerClass(List.class, ArrayList.class);
        registerClass(Map.class, HashMap.class);
    }

    /**
     * インターフェースの型のフィールドの初期化時に生成するクラスを登録します
     * 既に登録されているインターフェースの場合は上書きして登録されます
     * @param type 登録するインターフェース
     * @param typeClass 生成するクラス
     * @return メソッドチェーンに使用する自身のインスタンス
     * @throws IllegalArgumentException typeClassに渡されたクラスがインターフェースの場合
     */
    @NotNull
    public <T> ConvertOptions registerClass(Class<? super T> type, Class<T> typeClass) {
        if (typeClass.isInterface()) throw new IllegalArgumentException(typeClass.getName() + " is Interface");
        knownClass.put(type, typeClass);
        return this;
    }

    /**
     * このインスタンスに指定されたクラスが登録されているかを取得します
     * @param type 登録されているか確認するクラス
     * @return クラスが登録されている場合は true されていない場合は false
     */
    public boolean containsKey(Class<?> type) {
        return knownClass.containsKey(type);
    }

    @NotNull
    @SuppressWarnings("unchecked")
    <T> Class<T> getInstanceClass(Class<? super T> type) {
        if (!knownClass.containsKey(type)) {
            throw new ConvertException("No implementation class that resolves the '%s' is registered.".formatted(type.getName()));
        }
        return (Class<T>)knownClass.get(type);
    }

    /**
     * 変換時にキーが大文字小文字を区別するかを設定します。
     * @param ignoreCase falseの場合: キーの大文字小文字は区別されません。初期値:false
     * @return メソッドチェーンに使用する自身のインスタンス
     */
    @NotNull
    public ConvertOptions ignoreCase(boolean ignoreCase) {
        isIgnoreCase = ignoreCase;
        return this;
    }
}
