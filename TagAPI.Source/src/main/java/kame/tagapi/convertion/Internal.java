package kame.tagapi.convertion;

import java.lang.reflect.*;
import java.util.Arrays;

@SuppressWarnings({"unchecked", "unused"})
class Internal {

    private static final Object unsafe = get(getField(getClass("sun.misc.Unsafe"), "theUnsafe"), null);
    private static final Method alloc = getMethod(getClass("sun.misc.Unsafe"), "allocateInstance", Class.class);

    private static Class<?> getClass(String name) {
        try {
            return Class.forName(name);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> Constructor<T> getConstructor(Class<T> c, Class<?>... params) {
        try {
            var r = c.getDeclaredConstructor(params);
            if (!Modifier.isPublic(r.getModifiers())) r.setAccessible(true);
            return r;
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    private static Field getField(Class<?> c, String name) {
        try {
            var f = c.getDeclaredField(name);
            if (!Modifier.isPublic(f.getModifiers())) f.setAccessible(true);
            return f;
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    private static Field findField(Class<?> c, Class<?> type) {
        var f = Arrays.stream(c.getDeclaredFields())
                .filter(x -> !Modifier.isStatic(x.getModifiers()))
                .filter(x -> x.getType() == type)
                .findFirst()
                .orElseThrow();
        if (!Modifier.isPublic(f.getModifiers())) f.setAccessible(true);
        return f;
    }

    private static Method getMethod(Class<?> c, String name, Class<?>... args) {
        try {
            var m = c.getDeclaredMethod(name, args);
            if (!Modifier.isPublic(m.getModifiers())) m.setAccessible(true);
            return m;
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    private static Method findMethod(Class<?> c, Class<?> returnType, Class<?>... params) {
        var m = Arrays.stream(c.getDeclaredMethods())
                .filter(x -> !Modifier.isStatic(x.getModifiers()))
                .filter(x -> returnType.equals(x.getReturnType()))
                .filter(x -> Arrays.equals(x.getParameterTypes(), params))
                .findFirst()
                .orElseThrow();
        if (!Modifier.isPublic(m.getModifiers())) m.setAccessible(true);
        return m;
    }

    private static <T> T get(Field field, Object obj) {
        try {
            return (T) field.get(obj);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    static void set(Field field, Object obj, Object data) {
        try {
            field.set(obj, data);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("An exception occurred when trying to create an instance or set a field", e);
        }
    }

    private static <T> T invoke(Method method, Object obj, Object... params) {
        try {
            return (T) method.invoke(obj, params);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    static <T> T newInstance(Constructor<T> constructor, Object... params) {
        try {
            return constructor.newInstance(params);
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            throw new RuntimeException(e);
        }
    }

    static <T> T allocInstance(Class<T> type) {
        try {
            return (T) alloc.invoke(unsafe, type);
        } catch (InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException("An exception occurred when trying to create an instance or set a field", e);
        }
    }
}
