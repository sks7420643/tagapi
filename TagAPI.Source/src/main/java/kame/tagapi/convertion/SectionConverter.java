package kame.tagapi.convertion;

import com.google.common.base.Defaults;
import com.google.common.primitives.Primitives;
import kame.tagapi.TagBase;
import kame.tagapi.TagData;
import kame.tagapi.TagList;
import kame.tagapi.TagSection;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

@SuppressWarnings("unchecked")
public class SectionConverter {

    public static <T> T convertTo(TagBase tag, @NotNull Class<T> type) throws ConvertException {
        return convertTo(tag, type, ConvertOptions.DEFAULT);
    }

    public static <T> T convertTo(TagBase tag, @NotNull Class<T> type, ConvertOptions options) throws ConvertException {
        return convertTo(tag, new GenericClass<>(type, type), options);
    }

    private static <T> T convertTo(TagBase tag, GenericClass<T> gen, ConvertOptions options) throws ConvertException {
        if (TagBase.class.isAssignableFrom(gen.rawType())) {
            return gen.rawType().isInstance(tag) ? gen.rawType().cast(tag) : null;
        } else if (gen.rawType().isInterface()) {
            return convertTo(tag, new GenericClass<>(options.getInstanceClass(gen.rawType()), gen.type(), gen.generics()), options);
        } else if (tag instanceof TagData<?> data) {
            return gen.rawType().isInstance(data.getData()) ? gen.rawType().cast(data.getData()) : Defaults.defaultValue(Primitives.unwrap(gen.rawType()));
        } else if (tag instanceof TagSection sec && Map.class.isAssignableFrom(gen.rawType())) {
            return toMap(sec, gen, options);
        } else if (tag instanceof TagSection sec && gen.rawType().isRecord()) {
            return toRecord(sec, gen, options);
        } else if (tag instanceof TagSection sec) {
            return toClass(sec, gen, options);
        } else if (tag instanceof TagList list && gen.rawType().isArray()) {
            return toArray(list, gen, options);
        } else if (tag instanceof TagList list) {
            return toList(list, gen, options);
        } else {
            return Defaults.defaultValue(Primitives.unwrap(gen.rawType()));
        }
    }

    private static <T> T toClass(TagSection section, GenericClass<T> generic, ConvertOptions options) {
        try {
            var ret = Internal.allocInstance(generic.rawType());
            var map = generic.getGenericParameters();
            for (var field : generic.rawType().getDeclaredFields()) {
                field.setAccessible(true);
                var data = section.get(Reflect.getKeyName(field), options.isIgnoreCase);
                var generics = GenericClass.getGenericType(field.getGenericType(), map, generic.generics());
                field.set(ret, SectionConverter.convertTo(data, generics, options));
            }
            return ret;
        } catch (IllegalAccessException e) {
            throw new ConvertException("An exception occurred when trying to create an instance or set a field", e);
        }
    }

    private static <T> T toRecord(TagSection section, GenericClass<T> generic, ConvertOptions options) {
        var constructor = Reflect.getAnnotatedConstructors(generic.rawType(), null);
        if (constructor.isEmpty()) {
            throw new ConvertException("No constructor was found that can be used for serialization");
        }
        var params = constructor.get().getParameters();
        var map = generic.getGenericParameters();
        var list = new ArrayList<>();
        for (var param : params) {
            var data = section.get(Reflect.getKeyName(param), options.isIgnoreCase);
            var generics = GenericClass.getGenericType(param.getParameterizedType(), map, generic.generics());
            list.add(SectionConverter.convertTo(data, generics, options));
        }
        return Internal.newInstance(constructor.get(), list.toArray());
    }

    private static <T> T toMap(TagSection section, GenericClass<T> generic, ConvertOptions options) {
        var constructor = Reflect.getAnnotatedConstructors(generic.rawType(), new Class[0]);
        if (constructor.isEmpty()) {
            throw new ConvertException("No constructor was found that can be used for serialization");
        }
        var ret = Internal.newInstance(constructor.get());
        if (ret instanceof Map<?, ?>) {
            var map = (Map<String, Object>) ret;
            for (var entry : section.entrySet()) {
                var data = SectionConverter.convertTo(entry.getValue(), generic.generics()[1], options);
                if (data != null) {
                    map.put(entry.getKey(), data);
                }
            }
        }
        return ret;
    }

    private static <T> T toArray(TagList list, GenericClass<T> generic, ConvertOptions options) {
        var array = Array.newInstance(generic.rawType().componentType(), list.size());
        var map = generic.getGenericParameters();
        for (var i = 0; i < list.size(); i++) {
            Array.set(array, i, convertTo(list.get(i), GenericClass.getGenericType(generic.rawType().componentType(), map, generic.generics()), options));
        }
        return generic.rawType().cast(array);
    }

    private static <T> T toList(TagList list, GenericClass<T> generic, ConvertOptions options) {
        var constructor = Reflect.getAnnotatedConstructors(generic.rawType(), new Class[0]);
        if (constructor.isEmpty()) {
            throw new ConvertException("No constructor was found that can be used for serialization");
        }
        var ret = Internal.newInstance(constructor.get());
        if (ret instanceof Collection<?>) {
            if (generic.generics().length < 1) {
                throw new ConvertException("No referencable generic classes");
            }
            var collection = (Collection<Object>) ret;
            var genericClass = generic.generics()[0];
            for (var item : list) {
                collection.add(SectionConverter.convertTo(item, genericClass, options));
            }
        }
        return ret;
    }

}
