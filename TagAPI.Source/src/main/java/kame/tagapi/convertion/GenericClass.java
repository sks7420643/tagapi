package kame.tagapi.convertion;

import com.google.common.primitives.Primitives;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

record GenericClass<T>(Class<T> rawType, Type type, GenericClass<?>... generics) {

    public Map<String, Type> getGenericParameters() {
        var variables = getVariables(type);
        var parameters = Arrays.stream(generics).map(GenericClass::type).toArray(Type[]::new);
        var length = Math.min(variables.length, parameters.length);
        return IntStream.range(0, length).boxed().collect(Collectors.toMap(x -> variables[x].getName(), x -> parameters[x]));
    }

    private static TypeVariable<?>[] getVariables(Type type) {
        // TODO:インナークラスを考慮した設計にする
        if (type instanceof Class<?> clazz) {
            return clazz.getTypeParameters();
        } else if (type instanceof GenericArrayType array) {
            return getVariables(array.getGenericComponentType());
        } else if (type instanceof ParameterizedType param) {
            return getVariables(param.getRawType());
        } else if (type instanceof WildcardType wild) {
            return Arrays.stream(wild.getUpperBounds())
                    .map(GenericClass::getVariables)
                    .flatMap(Stream::of)
                    .toArray(TypeVariable[]::new);
        } else if (type instanceof TypeVariable<?> variable) {
            return new TypeVariable[]{variable};
        }
        return new TypeVariable[0];
    }

    static GenericClass<?> getGenericType(Type type, Map<String, Type> cl, GenericClass<?>[] generics) {
        if (type instanceof Class<?> clazz) {
            return new GenericClass<>(Primitives.wrap(clazz), clazz);
        } else if (type instanceof GenericArrayType array) {
            var item = getGenericType(array.getGenericComponentType(), cl, generics);
            var arrayType = Array.newInstance(item.rawType, 0).getClass();
            return new GenericClass<>(arrayType, type, item);
        } else if (type instanceof ParameterizedType param) {
            var rawType = (Class<?>) param.getRawType();
            var types = Arrays.stream(param.getActualTypeArguments()).map(x -> getGenericType(x, cl, generics));
            return new GenericClass<>(rawType, type, types.toArray(GenericClass[]::new));
        } else if (type instanceof WildcardType wild) {
            var list = new ArrayList<GenericClass<?>>();
            list.addAll(Arrays.stream(wild.getUpperBounds()).map(x -> getGenericType(x, cl, generics)).toList());
            list.addAll(Arrays.stream(wild.getLowerBounds()).map(x -> getGenericType(x, cl, generics)).toList());
            return list.stream().findFirst().orElseThrow();
        } else if (type instanceof TypeVariable<?> variable) {
            return getGenericType(cl.get(variable.getName()), cl, generics);
        }
        throw new RuntimeException();
    }
}