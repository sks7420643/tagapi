package kame.tagapi;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

/**
 * TagAPIによるNBTTagList値へのマッピングを持つクラスです。
 */
public class TagList extends AbstractList<TagBase> implements TagNode {

    final List<Object> nmsList;

    @SuppressWarnings({"unchecked", "unused"})
    TagList(Object nms) {
        nmsList = (List<Object>) nms;
    }

    /**
     * 空の情報を持ったTagListのインスタンスを作成します
     */
    public TagList() {
        nmsList = new ArrayList<>();
    }

    /**
     * 引数に渡した値で初期化したTagListのインスタンスを作成します
     * @param items 初期化に使用する値
     * @throws IllegalArgumentException 格納する要素が他の値と互換が無い場合に発生
     */
    public <T extends TagBase> TagList(Collection<T> items) {
        this();
        addAll(items);
    }

    /**
     * 指定したインデックスにある要素を取得します。
     * @param index 取得する要素のインデックス
     * @return 取得した要素
     */
    @Nullable
    @Override
    public TagBase get(int index) {
        return Mapper.toTag(nmsList.get(index));
    }

    /**
     * 指定したインデックスにある要素を取得します。
     * @param index 取得する要素のインデックス
     * @return 取得した要素、互換性の無い型の場合はnull
     */
    @Nullable
    public Byte getByte(int index) {
        return get(index) instanceof TagByte tag ? tag.data() : null;
    }

    /**
     * 指定したインデックスにある要素を取得します。
     * @param index 取得する要素のインデックス
     * @return 取得した要素、互換性の無い型の場合はnull
     */
    @Nullable
    public Short getShort(int index) {
        return get(index) instanceof TagShort tag ? tag.data() : null;
    }

    /**
     * 指定したインデックスにある要素を取得します。
     * @param index 取得する要素のインデックス
     * @return 取得した要素、互換性の無い型の場合はnull
     */
    @Nullable
    public Integer getInt(int index) {
        return get(index) instanceof TagInt tag ? tag.data() : null;
    }

    /**
     * 指定したインデックスにある要素を取得します。
     * @param index 取得する要素のインデックス
     * @return 取得した要素、互換性の無い型の場合はnull
     */
    @Nullable
    public Long getLong(int index) {
        return get(index) instanceof TagLong tag ? tag.data() : null;
    }

    /**
     * 指定したインデックスにある要素を取得します。
     * @param index 取得する要素のインデックス
     * @return 取得した要素、互換性の無い型の場合はnull
     */
    @Nullable
    public Float getFloat(int index) {
        return get(index) instanceof TagFloat tag ? tag.data() : null;
    }

    /**
     * 指定したインデックスにある要素を取得します。
     * @param index 取得する要素のインデックス
     * @return 取得した要素、互換性の無い型の場合はnull
     */
    @Nullable
    public Double getDouble(int index) {
        return get(index) instanceof TagDouble tag ? tag.data() : null;
    }

    /**
     * 指定したインデックスにある要素を取得します。
     * @param index 取得する要素のインデックス
     * @return 取得した要素、互換性の無い型の場合はnull
     */
    public byte @Nullable [] getByteArray(int index) {
        return get(index) instanceof TagByteArray tag ? tag.data() : null;
    }

    /**
     * 指定したインデックスにある要素を取得します。
     * @param index 取得する要素のインデックス
     * @return 取得した要素、互換性の無い型の場合はnull
     */
    public int @Nullable [] getIntArray(int index) {
        return get(index) instanceof TagIntArray tag ? tag.data() : null;
    }

    /**
     * 指定したインデックスにある要素を取得します。
     * @param index 取得する要素のインデックス
     * @return 取得した要素、互換性の無い型の場合はnull
     */
    public long @Nullable [] getLongArray(int index) {
        return get(index) instanceof TagLongArray tag ? tag.data() : null;
    }

    /**
     * 指定したインデックスにある要素を取得します。
     * @param index 取得する要素のインデックス
     * @return 取得した要素、互換性の無い型の場合はnull
     */
    @Nullable
    public TagList getList(int index) {
        return get(index) instanceof TagList tag ? tag : null;
    }

    /**
     * 指定したインデックスにある要素を取得します。
     * @param index 取得する要素のインデックス
     * @return 取得した要素、互換性の無い型の場合はnull
     */
    @Nullable
    public TagSection getSection(int index) {
        return get(index) instanceof TagSection tag ? tag : null;
    }

    /**
     * 指定したインデックスの要素を格納します。
     * @param index 格納する要素のインデックス
     * @param item 指定した位置に格納する要素
     * @return 置き換える前に格納されていた要素
     * @throws IllegalArgumentException 格納する要素が現在格納されている他の値と互換が無い場合に発生
     */
    @Nullable
    @Override
    public TagBase set(int index, TagBase item) {
        if(size() > 1)checkType(item);
        return Mapper.toTag(nmsList.set(index, Mapper.toNms(item)));
    }

    /**
     * 指定したインデックスの要素を格納します。
     * @param index 格納する要素のインデックス
     * @param item 指定した位置に格納する要素
     * @return 置き換える前に格納されていた要素
     * @throws IllegalArgumentException 格納する要素が現在格納されている他の値と互換が無い場合に発生
     */
    @Nullable
    public TagBase setByte(int index, byte item) {
        return set(index, new TagByte(item));
    }

    /**
     * 指定したインデックスの要素を格納します。
     * @param index 格納する要素のインデックス
     * @param item 指定した位置に格納する要素
     * @return 置き換える前に格納されていた要素
     * @throws IllegalArgumentException 格納する要素が現在格納されている他の値と互換が無い場合に発生
     */
    @Nullable
    public TagBase setShort(int index, short item) {
        return set(index, new TagShort(item));
    }

    /**
     * 指定したインデックスの要素を格納します。
     * @param index 格納する要素のインデックス
     * @param item 指定した位置に格納する要素
     * @return 置き換える前に格納されていた要素
     * @throws IllegalArgumentException 格納する要素が現在格納されている他の値と互換が無い場合に発生
     */
    @Nullable
    public TagBase setInt(int index, int item) {
        return set(index, new TagInt(item));
    }

    /**
     * 指定したインデックスの要素を格納します。
     * @param index 格納する要素のインデックス
     * @param item 指定した位置に格納する要素
     * @return 置き換える前に格納されていた要素
     * @throws IllegalArgumentException 格納する要素が現在格納されている他の値と互換が無い場合に発生
     */
    @Nullable
    public TagBase setLong(int index, long item) {
        return set(index, new TagLong(item));
    }

    /**
     * 指定したインデックスの要素を格納します。
     * @param index 格納する要素のインデックス
     * @param item 指定した位置に格納する要素
     * @return 置き換える前に格納されていた要素
     * @throws IllegalArgumentException 格納する要素が現在格納されている他の値と互換が無い場合に発生
     */
    @Nullable
    public TagBase setFloat(int index, float item) {
        return set(index, new TagFloat(item));
    }

    /**
     * 指定したインデックスの要素を格納します。
     * @param index 格納する要素のインデックス
     * @param item 指定した位置に格納する要素
     * @return 置き換える前に格納されていた要素
     * @throws IllegalArgumentException 格納する要素が現在格納されている他の値と互換が無い場合に発生
     */
    @Nullable
    public TagBase setDouble(int index, double item) {
        return set(index, new TagDouble(item));
    }

    /**
     * 指定したインデックスの要素を格納します。
     * @param index 格納する要素のインデックス
     * @param item 指定した位置に格納する要素
     * @return 置き換える前に格納されていた要素
     * @throws IllegalArgumentException 格納する要素が現在格納されている他の値と互換が無い場合に発生
     */
    @Nullable
    public TagBase setByteArray(int index, byte[] item) {
        return set(index, new TagByteArray(item));
    }

    /**
     * 指定したインデックスの要素を格納します。
     * @param index 格納する要素のインデックス
     * @param item 指定した位置に格納する要素
     * @return 置き換える前に格納されていた要素
     * @throws IllegalArgumentException 格納する要素が現在格納されている他の値と互換が無い場合に発生
     */
    @Nullable
    public TagBase setIntArray(int index, int[] item) {
        return set(index, new TagIntArray(item));
    }

    /**
     * 指定したインデックスの要素を格納します。
     * @param index 格納する要素のインデックス
     * @param item 指定した位置に格納する要素
     * @return 置き換える前に格納されていた要素
     * @throws IllegalArgumentException 格納する要素が現在格納されている他の値と互換が無い場合に発生
     */
    @Nullable
    public TagBase setLongArray(int index, long[] item) {
        return set(index, new TagLongArray(item));
    }

    /**
     * 指定したインデックスの要素を格納します。
     * @param index 格納する要素のインデックス
     * @param item 指定した位置に格納する要素
     * @return 置き換える前に格納されていた要素
     * @throws IllegalArgumentException 格納する要素が現在格納されている他の値と互換が無い場合に発生
     */
    @Nullable
    public TagBase setList(int index, TagList item) {
        return set(index, item);
    }

    /**
     * 指定したインデックスの要素を格納します。
     * @param index 格納する要素のインデックス
     * @param item 指定した位置に格納する要素
     * @return 置き換える前に格納されていた要素
     * @throws IllegalArgumentException 格納する要素が現在格納されている他の値と互換が無い場合に発生
     */
    @Nullable
    public TagBase setSection(int index, TagSection item) {
        return set(index, item);
    }

    /**
     * 現在のコレクションの最後に要素を格納します
     * @param item 格納する要素
     * @return この呼び出しによりコレクションの要素が変更された場合 true
     * @throws IllegalArgumentException 格納する要素が現在格納されている他の値と互換が無い場合に発生
     */
    @Override
    public boolean add(TagBase item) {
        checkType(item);
        return nmsList.add(Mapper.toNms(item));
    }

    /**
     * 指定したインデックスの位置に要素を挿入します
     * @param index 要素を挿入するインデックス
     * @param item 挿入する要素
     * @throws IllegalArgumentException 格納する要素が現在格納されている他の値と互換が無い場合に発生
     */
    @Override
    public void add(int index, TagBase item) {
        checkType(item);
        nmsList.add(index, Mapper.toNms(item));
    }

    /**
     * 現在のインスタンスの指定位置にある要素を削除します。
     * @param index 削除する要素のインデックス
     * @return 削除された子要素
     */
    @Nullable
    @Override
    public TagBase remove(int index) {
        return Mapper.toTag(nmsList.remove(index));
    }

    /**
     * 現在のインスタンスに含まれる子要素の数を取得します。
     * @return インスタンスに含まれる子要素の数
     */
    @Override
    public int size() {
        return nmsList.size();
    }

    /**
     * 現在のインスタンスの子要素の型を取得します。
     * @return インスタンスに含まれている子要素の型
     */
    @NotNull
    public Class<? extends TagBase> getElementType() {
        return Mapper.getListType(nmsList);
    }

    private void checkType(TagBase data) {
        if (!getElementType().isInstance(data)) {
            throw new IllegalArgumentException("Bad type:" + data.getClass());
        }
    }

    /**
     * 現在のインスタンスからNBT文字列を生成します。
     * @return 現在のインスタンスから生成されたNBT文字列
     */
    @Override
    public String toString() {
        return Mapper.toNms(this).toString();
    }
}
