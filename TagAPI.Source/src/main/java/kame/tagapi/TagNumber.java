package kame.tagapi;

/**
 * TagAPIによる数値を持つ定義を提供するインターフェースです。
 */
public interface TagNumber<T extends Number> extends TagData<T> {

}
