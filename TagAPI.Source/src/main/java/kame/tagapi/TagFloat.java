package kame.tagapi;

/**
 * TagAPIによるNBTTagFloat値へのマッピングを持つデータクラスです。
 * @param data NBTタグとして持つ値
 */
public record TagFloat(float data) implements TagNumber<Float> {

    /**
     * 現在のインスタンスからNBT文字列を生成します。
     * @return 現在のインスタンスから生成されたNBT文字列
     */
    public String toString() {
        return Mapper.toNms(this).toString();
    }

    /**
     * このインスタンスの値を取得します。
     * @return 内包されるデータ
     */
    @Override
    public Float getData() {
        return data;
    }
}
