package kame.tagapi;

import org.bukkit.inventory.ItemStack;

import java.util.Objects;

/**
 * アイテムのNBTを取得、編集する為の機能を提供します。
 */
public class TagItem {

    private final ItemStack edit;
    private final Object nmsItem;

    /**
     * ItemStackからこのインスタンスを初期化します。
     * @param item NBTを取得、編集するItemStackオブジェクト
     */
    public TagItem(ItemStack item) {
        edit = item;
        nmsItem = Mapper.asNMSCopy(item);
    }

    /**
     * このアイテムのルートにあるNBTデータを取得します。
     * このNBTで編集された内容はsaveメソッドを呼び出すまで反映されません。
     * @see #save()
     * @return 取得出来たNBT
     */
    public TagSection getSection() {
        return Mapper.toTag(Mapper.getNBTField(nmsItem)) instanceof TagSection data ? data : null;
    }

    /**
     * このインスタンスの初期化時に使用したItemStackを取得します。
     * @return このインスタンスの初期化時に使用したItemStackオブジェクト
     */
    public ItemStack getItemStack() {
        return edit;
    }

    /**
     * このインスタンスのNBTデータが元のItemStackから変更されたかを取得します。
     * @return NBTが変更されている場合はtrue、未変更の場合はfalse
     */
    public boolean hasChanged() {
        var meta = Mapper.getItemMeta(nmsItem);
        return !Objects.equals(edit.getItemMeta(), meta);
    }

    /**
     * このインスタンスのNBTオブジェクトで編集した内容を内包しているItemStackに反映します。
     * @return 保存前とNBTに差異があった場合はtrue、変更されなかった場合はfalse
     */
    public boolean save() {
        var meta = Mapper.getItemMeta(nmsItem);
        var result = Objects.equals(edit.getItemMeta(), meta);
        edit.setItemMeta(meta);
        return !result;

    }

}
