package kame.tagapi;

/**
 * NBTの配列機能を提供するインターフェースです。
 * @param <T> 配列の型
 */
public interface TagArray<T> extends TagData<T> {
    /**
     * 値を取得します。
     * @return 内包されている配列値
     */
    T data();
}
