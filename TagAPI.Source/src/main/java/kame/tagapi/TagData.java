package kame.tagapi;

/**
 * TagAPIによる値本体を持つ定義を提供するインターフェースです。
 */
public interface TagData<T> extends TagBase {

    /**
     * このインスタンスの値を取得します。
     * @return 内包されるデータ
     */
    T getData();
}
