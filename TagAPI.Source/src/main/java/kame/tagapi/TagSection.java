package kame.tagapi;

import kame.tagapi.convertion.ConvertOptions;
import kame.tagapi.convertion.SectionConverter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

/**
 * TagAPIによるNBTTagCompound値へのマッピングを持つクラスです。
 */
public class TagSection extends AbstractMap<String, TagBase> implements TagNode {

    final Map<String, Object> nmsMap;

    @SuppressWarnings({"unchecked", "unused"})
    TagSection(Object nms) {
        nmsMap = (Map<String, Object>) nms;
    }

    /**
     * 空の情報を持ったTagSectionのインスタンスを作成します
     */
    public TagSection() {
        nmsMap = new HashMap<>();
    }

    /**
     * 引数に渡した値で初期化したTagSectionのインスタンスを作成します
     * @param items 初期化に使用する値
     */
    public TagSection(Map<String, ? extends TagBase> items) {
        this();
        putAll(items);
    }

    /**
     * 指定したキーに要素を設定します。
     * @param key 関連付けるキー
     * @param value 指定したキーに設定される値
     * @return この呼び出しの前に設定されていた値、値が設定されていない場合はnull
     */
    @Nullable
    @Override
    public TagBase put(String key, TagBase value) {
        return Mapper.toTag(nmsMap.put(key, Mapper.toNms(value)));
    }

    /**
     * 指定したキーに要素を設定します。
     * @param key 関連付けるキー
     * @param value 指定したキーに設定される値
     * @return この呼び出しの前に設定されていた値、値が設定されていない場合はnull
     */
    @Nullable
    public TagBase setByte(@NotNull String key, byte value) {
        return put(key, new TagByte(value));
    }

    /**
     * 指定したキーに要素を設定します。
     * @param key 関連付けるキー
     * @param value 指定したキーに設定される値
     * @return この呼び出しの前に設定されていた値、値が設定されていない場合はnull
     */
    @Nullable
    public TagBase setShort(@NotNull String key, short value) {
        return put(key, new TagShort(value));
    }

    /**
     * 指定したキーに要素を設定します。
     * @param key 関連付けるキー
     * @param value 指定したキーに設定される値
     * @return この呼び出しの前に設定されていた値、値が設定されていない場合はnull
     */
    @Nullable
    public TagBase setInt(@NotNull String key, int value) {
        return put(key, new TagInt(value));
    }

    /**
     * 指定したキーに要素を設定します。
     * @param key 関連付けるキー
     * @param value 指定したキーに設定される値
     * @return この呼び出しの前に設定されていた値、値が設定されていない場合はnull
     */
    @Nullable
    public TagBase setLong(@NotNull String key, long value) {
        return put(key, new TagLong(value));
    }

    /**
     * 指定したキーに要素を設定します。
     * @param key 関連付けるキー
     * @param value 指定したキーに設定される値
     * @return この呼び出しの前に設定されていた値、値が設定されていない場合はnull
     */
    @Nullable
    public TagBase setFloat(@NotNull String key, float value) {
        return put(key, new TagFloat(value));
    }

    /**
     * 指定したキーに要素を設定します。
     * @param key 関連付けるキー
     * @param value 指定したキーに設定される値
     * @return この呼び出しの前に設定されていた値、値が設定されていない場合はnull
     */
    @Nullable
    public TagBase setDouble(@NotNull String key, double value) {
        return put(key, new TagDouble(value));
    }

    /**
     * 指定したキーに要素を設定します。
     * @param key 関連付けるキー
     * @param value 指定したキーに設定される値
     * @return この呼び出しの前に設定されていた値、値が設定されていない場合はnull
     */
    @Nullable
    public TagBase setByteArray(@NotNull String key, byte @NotNull [] value) {
        return put(key, new TagByteArray(value));
    }

    /**
     * 指定したキーに要素を設定します。
     * @param key 関連付けるキー
     * @param value 指定したキーに設定される値
     * @return この呼び出しの前に設定されていた値、値が設定されていない場合はnull
     */
    @Nullable
    public TagBase setIntArray(@NotNull String key, int @NotNull [] value) {
        return put(key, new TagIntArray(value));
    }

    /**
     * 指定したキーに要素を設定します。
     * @param key 関連付けるキー
     * @param value 指定したキーに設定される値
     * @return この呼び出しの前に設定されていた値、値が設定されていない場合はnull
     */
    @Nullable
    public TagBase setLongArray(@NotNull String key, long @NotNull [] value) {
        return put(key, new TagLongArray(value));
    }

    /**
     * 指定したキーに要素を設定します。
     * @param key 関連付けるキー
     * @param value 指定したキーに設定される値
     * @return この呼び出しの前に設定されていた値、値が設定されていない場合はnull
     */
    @Nullable
    public TagBase setList(@NotNull String key, TagList value) {
        return put(key, value);
    }

    /**
     * 指定したキーに要素を設定します。
     * @param key 関連付けるキー
     * @param value 指定したキーに設定される値
     * @return この呼び出しの前に設定されていた値、値が設定されていない場合はnull
     */
    @Nullable
    public TagBase setSection(@NotNull String key, TagSection value) {
        return put(key, value);
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @return 指定したキーに関連付けられている要素
     */
    @Nullable
    @Override
    public TagBase get(Object key) {
        return Mapper.toTag(nmsMap.get(key));
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @param ignoreCase キーの大文字小文字を無視する場合はtrueを指定
     * @return 指定したキーに関連付けられている要素
     */
    @Nullable
    public TagBase get(Object key, boolean ignoreCase) {
        for (var entry : nmsMap.entrySet()) {
            if(key instanceof String str && entry.getKey().equalsIgnoreCase(str)) {
                return Mapper.toTag(entry.getValue());
            }
        }
        return null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public Byte getByte(@NotNull String key) {
        return get(key) instanceof TagByte tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @param ignoreCase キーの大文字小文字を無視する場合はtrueを指定
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public Byte getByte(@NotNull String key, boolean ignoreCase) {
        return get(key, ignoreCase) instanceof TagByte tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public Short getShort(@NotNull String key) {
        return get(key) instanceof TagShort tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @param ignoreCase キーの大文字小文字を無視する場合はtrueを指定
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public Short getShort(@NotNull String key, boolean ignoreCase) {
        return get(key, ignoreCase) instanceof TagShort tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public Integer getInt(@NotNull String key) {
        return get(key) instanceof TagInt tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @param ignoreCase キーの大文字小文字を無視する場合はtrueを指定
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public Integer getInt(@NotNull String key, boolean ignoreCase) {
        return get(key, ignoreCase) instanceof TagInt tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public Long getLong(@NotNull String key) {
        return get(key) instanceof TagLong tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @param ignoreCase キーの大文字小文字を無視する場合はtrueを指定
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public Long getLong(@NotNull String key, boolean ignoreCase) {
        return get(key, ignoreCase) instanceof TagLong tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public Float getFloat(@NotNull String key) {
        return get(key) instanceof TagFloat tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @param ignoreCase キーの大文字小文字を無視する場合はtrueを指定
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public Float getFloat(@NotNull String key, boolean ignoreCase) {
        return get(key, ignoreCase) instanceof TagFloat tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public Double getDouble(@NotNull String key) {
        return get(key) instanceof TagDouble tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @param ignoreCase キーの大文字小文字を無視する場合はtrueを指定
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public Double getDouble(@NotNull String key, boolean ignoreCase) {
        return get(key, ignoreCase) instanceof TagDouble tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    public byte @Nullable [] getByteArray(@NotNull String key) {
        return get(key) instanceof TagByteArray tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @param ignoreCase キーの大文字小文字を無視する場合はtrueを指定
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    public byte @Nullable [] getByteArray(@NotNull String key, boolean ignoreCase) {
        return get(key, ignoreCase) instanceof TagByteArray tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    public int @Nullable [] getIntArray(@NotNull String key) {
        return get(key) instanceof TagIntArray tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @param ignoreCase キーの大文字小文字を無視する場合はtrueを指定
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    public int @Nullable [] getIntArray(@NotNull String key, boolean ignoreCase) {
        return get(key, ignoreCase) instanceof TagIntArray tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    public long @Nullable [] getLongArray(@NotNull String key) {
        return get(key) instanceof TagLongArray tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @param ignoreCase キーの大文字小文字を無視する場合はtrueを指定
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    public long @Nullable [] getLongArray(@NotNull String key, boolean ignoreCase) {
        return get(key, ignoreCase) instanceof TagLongArray tag ? tag.data() : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public TagList getList(@NotNull String key) {
        return get(key) instanceof TagList tag ? tag : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @param ignoreCase キーの大文字小文字を無視する場合はtrueを指定
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public TagList getList(@NotNull String key, boolean ignoreCase) {
        return get(key, ignoreCase) instanceof TagList tag ? tag : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public TagSection getSection(@NotNull String key) {
        return get(key) instanceof TagSection tag ? tag : null;
    }

    /**
     * 指定したキーに関連付けられている要素を取得します。
     * @param key 取得する要素のキー
     * @param ignoreCase キーの大文字小文字を無視する場合はtrueを指定
     * @return 指定したキーに関連付けられている要素、キーに関連付けられていない場合や互換性の無い型の場合はnull
     */
    @Nullable
    public TagSection getSection(@NotNull String key, boolean ignoreCase) {
        return get(key, ignoreCase) instanceof TagSection tag ? tag : null;
    }

    /**
     * 指定したキーに空のTagSectionのインスタンスを作成します。
     * @param key インスタンスを作成して設定するキー
     * @return 作成に成功、または、既に互換性のある型の値が存在した場合はその値、互換性の無い値が既に存在した場合はnull
     */
    @Nullable
    public TagSection createSection(String key) {
        if (!nmsMap.containsKey(key)) {
            var ret = new TagSection();
            put(key, ret);
            return ret;
        } else {
            return Mapper.toTag(nmsMap.get(key)) instanceof TagSection ret ? ret : null;
        }
    }

    /**
     * 指定したキーに空のTagListのインスタンスを作成します。
     * @param key インスタンスを作成して設定するキー
     * @return 作成に成功、または、既に互換性のある型の値が存在した場合はその値、互換性の無い値が既に存在した場合はnull
     */
    @Nullable
    public TagList createList(String key) {
        if (!nmsMap.containsKey(key)) {
            var ret = new TagList();
            put(key, ret);
            return ret;
        } else {
            return Mapper.toTag(nmsMap.get(key)) instanceof TagList ret ? ret : null;
        }
    }

    /**
     * 指定したキーに関連付けられている要素を削除します。
     * @param key 削除する要素のキー
     * @return 指定したキーに値が存在していた場合はその値
     */
    @Nullable
    @Override
    public TagBase remove(Object key) {
        return Mapper.toTag(nmsMap.remove(key));
    }

    /**
     * 現在のインスタンスからNBT文字列を生成します。
     * @return 現在のインスタンスから生成されたNBT文字列
     */
    @Override
    public String toString() {
        return Mapper.toNms(this).toString();
    }

    /**
     * 現在のインスタンスから指定したクラスの生成を行います。
     * @param type 変換する型情報
     * @param <T>  変換する型
     * @return 現在のインスタンスから指定した型に変換した値
     */
    public <T> T convertTo(Class<T> type) {
        return SectionConverter.convertTo(this, type);
    }

    /**
     * 現在のインスタンスから指定したクラスの生成を行います。
     * @param type    変換する型情報
     * @param options 変換を行う時のオプション
     * @param <T>     変換する型
     * @return 現在のインスタンスから指定した型に変換した値
     */
    public <T> T convertTo(Class<T> type, ConvertOptions options) {
        return SectionConverter.convertTo(this, type, options);
    }

    /**
     * このインスタンスに含まれるキーと値の集合を返します。
     * この呼び出しで取得された集合に対しての変更はこのインスタンスにも反映されます。
     * @return このインスタンスに含まれるキーと値の集合
     */
    @NotNull
    @Override
    public Set<Entry<String, TagBase>> entrySet() {
        return new AbstractSet<>() {
            @Override
            public Iterator<Entry<String, TagBase>> iterator() {
                var iterator = TagSection.this.nmsMap.entrySet().iterator();
                return new Iterator<>() {
                    @Override
                    public boolean hasNext() {
                        return iterator.hasNext();
                    }

                    @Override
                    public Entry<String, TagBase> next() {
                        var next = iterator.next();
                        return new SimpleEntry<>(next.getKey(), Mapper.toTag(next.getValue()))
                        {
                            @Override
                            public TagBase setValue(TagBase value) {
                                next.setValue(Mapper.toNms(value));
                                return super.setValue(value);
                            }
                        };
                    }

                    @Override
                    public void remove() {
                        iterator.remove();
                    }
                };
            }

            @Override
            public int size() {
                return TagSection.this.nmsMap.size();
            }
        };
    }
}
