package kame.tagapi;

import org.bukkit.block.Block;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

/**
 * タイルエンティティのNBTを取得、編集する為の機能を提供します。
 */
public class TagTileEntity {

    private final Object entity;

    private TagBase section;

    /**
     * 引数に指定したBlockからこのインスタンスを初期化します。
     * @param block NBTを取得、編集するBlock(TileEntity)オブジェクト
     */
    public TagTileEntity(Block block) {
        this.entity = Mapper.getTileEntity(block);
        if (entity != null) {
            section = Mapper.toTag(Mapper.getTileEntityData(entity));
        }
    }

    /**
     * このブロックの状態をNBTデータとして取得します。
     * ブロックがTileEntityでない場合はこのメソッドはnullを返します。
     * このNBTで編集された内容はsaveメソッドを呼び出すまで反映されません。
     * @see #save()
     * @return 取得出来たNBT。
     */
    public @Nullable TagSection getSection() {
        return section instanceof TagSection sec ? sec : null;
    }

    /**
     * このインスタンスのNBTデータが元の状態から変更されたかを取得します。
     * @return NBTが変更されている場合はtrue、未変更の場合はfalse
     */
    public boolean hasChanged() {
        if (entity == null) { return false; }
        var nbt = Mapper.getTileEntityData(entity);
        var change = Mapper.toNms(section);
        return !Objects.equals(nbt, change);
    }

    /**
     * このインスタンスのNBTオブジェクトで編集した内容を内包しているEntityに反映します。
     * @return 保存前とNBTに差異があった場合はtrue、変更されなかった場合はfalse
     */
    public boolean save() {
        if (hasChanged()) {
            Mapper.setTileEntityData(entity, Mapper.toNms(section));
            return true;
        } else {
            return false;
        }
    }
}
