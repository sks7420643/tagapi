package kame.tagapi;

import org.bukkit.inventory.ItemStack;

/**
 * TagAPIによるNBTの機能を提供するインターフェースのルートです。
 * TagAPIによって提供される全てのNBT操作クラスはTagBaseを実装します。
 */
public interface TagBase {

    /**
     * 文字列からTagBaseのインスタンスを生成します。
     * 生成されたインスタンスの型は引数として渡された文字列に依存します。
     * @param nbtString TagBaseインスタンスに変換するNBTの文字列
     * @return 文字列から生成されたインスタンス。
     * @throws Exception 文字列の解析に失敗した場合等に発生。
     */
    static TagBase parse(String nbtString) throws Exception {
        return Mapper.parse(nbtString);
    }

    /**
     * ItemStackインスタンスを内包するTagItemインスタンスを生成します。
     * @param item TagItemを生成するItemStack
     * @return 生成されたTagItemインスタンス
     */
    static TagItem from(ItemStack item) {
        return new TagItem(item);
    }

    /**
     * 現在のインスタンスからNBT文字列を生成します。
     * @return 現在のインスタンスから生成されたNBT文字列
     */
    default String getNBTString() {
        return Mapper.toNms(this).toString();
    }
}
