package kame.tagapi;

/**
 * TagAPIによる子要素を持つ定義を提供するインターフェースです。
 */
public interface TagNode extends TagBase {

    /**
     * このオブジェクトに含まれている子要素の数を取得します。
     * @return このオブジェクトに含まれている子要素の数
     */
    int size();
}
